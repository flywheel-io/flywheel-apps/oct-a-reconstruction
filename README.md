# oct-a-reconstruction

This gear generates a OCT-Angiography (OCT-A) from a retinal Optical Coherence Tomography volume with shape (Number of slices x Number of Columns in Slice x Number of Rows in Slice).